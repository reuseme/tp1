from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from userhandle.models import ReUseUser


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    picture_profile = forms.ImageField(required=False)
    password1 = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "pass_1",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your Password",
                "autocomplete" : "true",
                }
    ))
    password2 = forms.CharField(label='', widget=forms.PasswordInput(
        attrs = {
                "id" : "pass_2",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Type Your Password Again",
                "autocomplete" : "true",
                }
    ))

    email = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "email_sign",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your Email",
                }
    ))

    username = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "username_sign",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your Username",
                }
    ))

    no_identitas = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "identity_sign",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your Identity",
                }
    ))
    
    alamat = forms.CharField(label="", widget=forms.Textarea(
        attrs = {
                "id" : "address_sign",
                "style" : "width: 35%; height: 200px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your Address",
                }
    ))

    nama_bank = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "bank_sign",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your Bank",
                }
    ))

    no_rekening = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "acc_number_sign",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your Account Number",
                }
    ))
    nama_panjang = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "fullname_sign",
                "style" : "width: 35%; height: 65px; padding: 12px; border: 1px solid black; \
                            border-radius: 35px; box-sizing: border-box; resize: vertical; \
                            margin-top: 23px; ",
                "placeholder" : "Your FullName",
                }
    ))

    class Meta:
        model = ReUseUser
        fields = ('picture_profile', 'no_identitas', 'username','password1', 'password2', 'email', 
            'alamat', 'nama_bank', 'no_rekening','nama_panjang')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    email = forms.EmailField(label="", widget=forms.TextInput(
        attrs = {
                "id" : "email-inline",
                "name" : "email",
                            
                }
    ))

    
    class Meta:
        model = ReUseUser
        fields = ('email', 'username','picture_profile', 'no_identitas','alamat','nama_bank', 'no_rekening')

    # def clean_password(self):
    #     # Regardless of what the user provides, return the initial value.
    #     # This is done here, rather than on the field, because the
    #     # field does not have access to the initial value
    #     return self.initial['password']


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'username', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password', 'no_rekening', 'nama_panjang')}),
        ('Personal info', {'fields': ('username','picture_profile')}),
        ('Permissions', {'fields': ('is_admin',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username','picture_profile', 'password1', 'password2')}
        ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()

# Now register the new UserAdmin...
admin.site.register(ReUseUser, UserAdmin)

# ... and, since we're not using Django's built-in permissions,
# unregister the Group model from admin.
admin.site.unregister(Group)