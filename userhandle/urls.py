from django.urls import path
from userhandle.views import *
app_name = "user"

urlpatterns = [
    path("masuk", masuk_views, name="masuk"),
    path("daftar", daftar_views, name="daftar"),
    path("keluar", keluar_views, name="keluar"),
    path("profile/<username>", profile_views, name="profile"),
]