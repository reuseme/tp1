from django.shortcuts import render,redirect, HttpResponseRedirect
from django.http import HttpResponseBadRequest
from django.http import HttpResponse
from userhandle.admin import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login,authenticate,logout
from django.urls import reverse
from baranghandle.models import Keranjang
from userhandle.models import ReUseUser

def daftar_views(request):
    import base64
    
    form = UserCreationForm(request.POST)
    if request.method == "POST":
        
        if form.is_valid():
            
            user = form.save(commit=False)  #buat object untuk ReUseUser
            user.set_password(form.cleaned_data['password1'])
            user.picture_profile = form.cleaned_data['picture_profile']
            try:
                with form.cleaned_data["picture_profile"].open('rb') as file:
                    print('ada')
                    user.base64_pic = "data:image/png;base64,{}".format(base64.b64encode(file.read()).decode())
                    user.save()
            except:
                print('engga')
                user.base64_pic = open('userhandle/static/img/default_img', 'r').read()
                user.save()

            instance = Keranjang(pembeli=user)
            instance.save()

            return redirect("user:masuk")
        else :
            messages.warning(request, form.errors)
    
    return render(request, "userhandle/daftar.html",
            {
                "form" : UserCreationForm() 
            }
        )

def masuk_views(request):

    if request.method == "POST" :
        
        email = request.POST.get("email")     # grab the input from the user
        password = request.POST.get("password")

        if email == "" or password == "" :
            messages.warning(request, "Please Input Your Username and Password")
            return HttpResponseRedirect(reverse("user:masuk"))
        else :
            
            user = authenticate(email = email, password = password)   # return true if username and pass verified
            
            if user :
                if user.is_active :
                    login(request, user)
                    name = request.user
                    
                    return HttpResponseRedirect(reverse("barang:home")) 
                
                else :
                    messages.warning(request, "Invalid username or password")
                    return HttpResponse("the username or password are invalid")
            
            else :
                print("there any user failed login")
                print("email: {} and pass: {}".format(email,password))
                messages.warning(request, "Invalid username or password")
                return HttpResponseRedirect(reverse("user:masuk"))
    
    else :
        return render(request, "userhandle/masuk.html")

@login_required
def keluar_views(request) :
    logout(request)
    return HttpResponseRedirect(reverse("user:keluar"))

@login_required
def profile_views(request, username) :
    profile_detail = ReUseUser.objects.get(username=username)

    return render(request, "userhandle/profile.html", {
        "profile" : profile_detail,
    })