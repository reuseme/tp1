from django.test import TestCase, Client
from userhandle.models import ReUseUser
from userhandle.admin import UserCreationForm

class TestModel(TestCase):

    def setUp(self):
        ReUseUser.objects.create(
            nama_panjang = "anggardha febriano",
            no_identitas = "123456789",
            username = "anggardhanoano",
            password = "test123",
            email = "anggardhanoano@gmail.com",
            alamat = "pesona depok 1,blok ad 2",
            nama_bank = "Bank Negara Berkembang",
            no_rekening = "000123456"
        )

    def test_ModelObject(self):
        model_object = ReUseUser.objects.get(nama_panjang="anggardha febriano")
        self.assertEqual(model_object.nama_panjang,"anggardha febriano")
        self.assertEqual(model_object.no_identitas,"123456789")
        self.assertEqual(model_object.username,"anggardhanoano")
        self.assertEqual(model_object.password, "test123")
        self.assertEqual(model_object.email,"anggardhanoano@gmail.com")
        self.assertEqual(model_object.alamat,"pesona depok 1,blok ad 2")
        self.assertEqual(model_object.nama_bank,"Bank Negara Berkembang")
        self.assertEqual(model_object.no_rekening,"000123456")

class TestViews(TestCase):

    def test_url_signup(self):
        self.client = Client()
        response = self.client.get("/user/daftar")
        self.assertEqual(response.status_code, 200)
    
    def test_url_login(self):
        self.client = Client()
        response = self.client.get("/user/masuk")
        self.assertEqual(response.status_code, 200)
    
    def test_valid_data(self):
        data = {
            "email":"test@gmail.com",
            "username":"test123",
            "password1":"test",
            "password2":"test",
            "nama_panjang":"testing",
            "no_identitas":"123456789",
            "nama_bank" : "bank negara berkembang",
            "no_identitas" : "123457876545",
            "alamat" : "pesona depok 1",
            "picture_profile" : "/",
            "no_rekening" : "123456",
        }
        response = Client().post("/user/daftar", data)
        self.assertEqual(response.status_code, 302)


    def test_login(self):
        data = {
            "email":"test@gmail.com",
            "password":"test"
        }
        response = Client().post("/user/masuk", data)
        self.assertEqual(response.status_code, 302)
        
