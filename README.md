# TP1 + TP2 

branch developing for staging and master for final (sering ada hal2 kecil yg diedit langsung di master atau developing)
maaf jika pembagian tugas kurang tedefinisi karena banyak tugas TP2 yang sudah selesai di TP1

**Fitur Reusable**
*  login biasa, login google
*  pembelian barang
*  pencarian barang berdasarkan nama atau kategori
*  menjual barang
*  homepage berisi barang yang belum terbeli (jika sudah terbeli maka hilang dari homepage)
*  tidak bisa membeli barang yang dia jual
*  user yang sudah login dapat menulis testimoni di halaman about
*  user di sapa ketika sudah login di navbar
*  dapat me-list semua barang yang sudah dijual
*  dapat me-list semua barang yang sudah dibeli
*  dapat melihat akun profile tiap2 user
*  session login bertahan selama 30 menit

**Anggota kelompok:**
1. Ahmad Irfan
2. Anggardha Febriano
3. Muhammad Ariq Basyar
4. Rizki Leonardo

**Status pipelines:**
[![pipeline status](https://gitlab.com/reuseme/tp1/badges/master/pipeline.svg)](https://gitlab.com/reuseme/tp1/commits/master)

**Status code coverage:**
[![coverage report](https://gitlab.com/reuseme/tp1/badges/master/coverage.svg)](https://gitlab.com/reuseme/tp1/commits/master)

**Link heroku:**
http://reusable.herokuapp.com/

**Kontribusi Fitur**

**Ahmad Irfan (branch mamat)**
*  App about(models, views, tests, etc)
*  html template for homepage, detail barang, list barang, list pembelian
*  styling
*  Testing

**Anggardha Febriano (branch angga & remoteangga)**
*  models for baranghandle and userhandle
*  views for userhandle and baranghandle
*  loader for asynchronous
*  wireframe
*  Testing

**Muhammad Ariq Basyar (branch ariqq)**
*  javascripts and ajax flow for asynchronous (at homepage, list barang, and list pembelian)
*  views for baranghandle and userhandle (for ajax and base64 flow)
*  models userhandle
*  Google Oauth
*  figma
*  Testing
*  readme

**Rizki Leonardo (branch dev_leo)**
*  persona
*  Testing

**Keterangan**
*  file - file static berada di dalam folder praktikum
*  file static terdefinisi dan tersimpan dengan baik yang selanjutnya akan masuk ke staticfiles (collectstatic)
*  css framework menggunakan materialize
*  menggunakan jquery versi 3.1.0
*  menggunakan plugin javascript tambahan dari materialize
*  loader terdapat di homepage dengan tujuan untuk loading asynchronous ajax
*  efek animasi terdefinisi dengan baik yaitu fade out fake class setelah menutup hasil pencarian di homepage
*  efek animasi juga terdapat pada about app yaitu di anchor tag "about us", menggunakan waves-effect
*  django template terdefinisi dengan baik, menggunakan block dan endblock untuk head, body, dan javascript
