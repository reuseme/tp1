from django.contrib import admin
from django.urls import path, include
from .views import error_404
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
from django.conf.urls import handler404, handler500
from django.conf import settings
from django.contrib.staticfiles.urls import static
from django.conf.urls import url

app_name = "root"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include("baranghandle.urls")),
    path('account/', include('social_django.urls', namespace='social')),
    path('user/', include("userhandle.urls")),
    path('about/', include("about.urls")),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon/favicon.ico')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = error_404
