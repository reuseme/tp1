$(document).ready(function () {
    $(".close").click(function () {
        $(".fake").hide("200")
        $(".Modal").hide("500");
    });
    ajaxnya()
});

function tutup() {
    $(".fake").fadeOut();
    $("#isinya").remove();
}
function escapeHtml(unsafe) {
    return unsafe
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
}

$("#formq").submit(function (e) {
    e.preventDefault();
    var csrftoken = $('[name="csrfmiddlewaretoken"]').attr('value')
    var isi = escapeHtml($("#qinput").val())
    $("#isinya").remove()
    if (isi.trim().length > 0) {
        $.ajax({
            method: 'POST',
            URL: '/',
            data: {
                'q': isi
            },
            headers: {
                "X-CSRFToken": csrftoken,
            },
            success: function (result) {
                var result = result.data;
                $("#divnya").append('<div id="isinya"></div>');
                var isi_wrap =
                '<div class="fake"></div>' +
                '<div class="wrap">' +
                '<div class="Modal card" id="modal_card">' +
                '<button class="btn btn-flat close right" onclick="tutup()"><i class="material-icons">close</i></button>' +
                '<h3 class="center-align">Hasil Pencarian "' + isi + '"</h3>' +
                '</div>' +
                '</div>'
                $("#isinya").append(isi_wrap);
                if (result.length > 0) {
                    $.each(result, function (index, val) {
                        var data =
                        '<div class="col s4" style="height:30em ;width:20em">' +
                        '<div class="card">' +
                        '<div class="card-image">' +
                        '<img src="' + val.base64_pic +
                        '" style="width:100%; height:20em">' +
                        '<a class="btn-floating halfway-fab waves-effect waves-light red" href="/detail_barang/' +
                        val.slug + '"><i class="material-icons">add</i></a>' +
                        '</div>' +
                        '<div class="card-content">' +
                        '<p><span class="card-title">' + val.nama_barang + '</span></p>' +
                        '<p>Penjual   = ' + val.penjual + '</p>' +
                        '<p>Kategori   = ' + val.kategori_barang + '</p>' +
                        '<p>Harga    = Rp ' + val.harga + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                        
                        $("#modal_card").append(data)
                    })
                } else {
                    $("#modal_card").append('<h1 class="center-align">Kosong</h1>')
                }
            },
            error: function (a, b) {
                console.log(a);
                console.log(b);
            }
        })
    }
})
$("#formcategory").submit(function (e) {
    e.preventDefault();
    var csrftoken = $('[name="csrfmiddlewaretoken"]').attr('value')
    var isi = escapeHtml($("#category").val())
    $("#isinya").remove()
    if (isi.trim().length > 0) {
        $.ajax({
            method: 'POST',
            URL: '/',
            data: {
                'kategori': isi
            },
            headers: {
                "X-CSRFToken": csrftoken,
            },
            success: function (result) {
                var result = result.data;
                $("#divnya").append('<div id="isinya"></div>');
                var isi_wrap =
                '<div class="fake"></div>' +
                '<div class="wrap">' +
                '<div class="Modal card" id="modal_card">' +
                '<button class="btn btn-flat close right" onclick="tutup()"><i class="material-icons">close</i></button>' +
                '<h3 class="center-align">Hasil Pencarian "' + isi + '"</h3>' +
                '</div>' +
                '</div>'
                $("#isinya").append(isi_wrap);
                if (result.length > 0) {
                    $.each(result, function (index, val) {
                        var data =
                        '<div class="col s4" style="height:30em ;width:20em">' +
                        '<div class="card">' +
                        '<div class="card-image">' +
                        '<img src="' + val.base64_pic + '" style="width:100%; height:20em">' +
                        '<a class="btn-floating halfway-fab waves-effect waves-light red" href="/detail_barang/' +
                        val.slug + '"><i class="material-icons">add</i></a>' +
                        '</div>' +
                        '<div class="card-content">' +
                        '<p><span class="card-title">' + val.nama_barang + '</span></p>' +
                        '<p>Penjual   = ' + val.penjual + '</p>' +
                        '<p>Kategori   = ' + val.kategori_barang + '</p>' +
                        '<p>Harga    = Rp ' + val.harga + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                        
                        $("#modal_card").append(data)
                    })
                } else {
                    $("#modal_card").append('<h1 class="center-align">Kosong</h1>')
                }
            },
            error: function (a, b) {
                console.log(a);
                console.log(b);
            }
        })
    }
})

function ajaxnya() {
    const load = document.getElementsByClassName("loader")[0]
    $.ajax({
        method: 'GET',
        url: '/barang.json',
        success: function (result) {
            var result = result.data;
            if (result.length > 0) {
                $.each(result, function (index, val) {
                    var data =
                    '<div class="col s12 m4 l4 xl4" style="height:30em">' +
                    '<div class="card">' +
                    '<div class="card-image">' +
                    '<img src="' + val.base64_pic + '" style="width:100%; height:20em">' +
                    '<a class="btn-floating halfway-fab waves-effect waves-light red" href="/detail_barang/' +
                    val.slug + '"><i class="material-icons">add</i></a>' +
                    '</div>' +
                    '<div class="card-content">' +
                    '<p><span class="card-title">' + val.nama_barang + '</span></p>' +
                    '<p>Penjual   = <span>' + val.penjual + '</span></p>' +
                    '<p>Kategori   = ' + val.kategori_barang + '</p>' +
                    '<p>Harga   = Rp ' + val.harga + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                    $("#divnya2").append(data)
                    load.style.display = "none";
                })
            } else {
                $("#divnya2").append('<h1>Masih belum ada barang</h1>')
                load.style.display = "none"
            }
        },
        error: function (a, b) {
            console.log(a);
            console.log(b);
        }
    })
}