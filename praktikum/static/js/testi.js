$("#submit_testi").attr('onclick', 'submitTesti()')
$(document).ready(function () {
    ajaxnya()
})

function submitTesti() {
    const input = document.getElementById("testimoni")
    if (input.value.trim().length > 0){
        $.ajax({
            type: "POST",
            url: "/about/submit_testi",
            data: {
                testimoni: input.value,
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
            },
            success: function (result) {
                input.value = "";
                var result = result.data
                alert("terimakasih atas testimoni nya :)")
                $("#testinya").remove()
                render(result)
            }
        })
    } else {
        alert("masukkan testimoni dengan benar")
    }
}

function ajaxnya() {
    $.ajax({
        method: 'GET',
        url: '/about/submit_testi',
        success: function (result) {
            var result = result.data;
            render(result);
        },
        error: function (a, b) {
            console.log(a)
            console.log(b)
        }
    })
}

function render(someArray) {
    $("#isinya").append('<div id="testinya"></div>')
    console.log(someArray)
    if (someArray.length > 0) {
        $.each(someArray, function (index, val) {
            var isi =
            '<div class="col s12 m6 l3" style="background-color: #418CE3; margin: 1%; border-radius: 4px;">' +
            '<p style="padding: 3px; color: #418CE3; background-color: white; border-radius: 3px;">"' +
            val.testimoni + '"</p>' +
            '<p style="color: white"> - ' + val.penulis + '</p>' +
            '</div>';
            $("#testinya").append(isi)
        })
        
    } else {
        $("#testinya").append('<h1>Belum ada testimoni</h1>')
    }
    $("#testinya").append('<br>')
}


document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.modal');
    var instances = M.Modal.init(elems, options);
});

// Or with jQuery

$(document).ready(function(){
    $('.modal').modal();
});