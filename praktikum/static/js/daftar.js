function upload_img(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img_id').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function () {
    document.getElementById("id_picture_profile").setAttribute("onchange", "upload_img(this)");
});