$(document).ready(function () {
    ajaxnya()
})

function ajaxnya() {
    var csrftoken = $('[name="csrfmiddlewaretoken"]').attr('value')
    $.ajax({
        method: 'POST',
        url: '/list_barang',
        data: {
            'data': $("#user").attr("data")
        },
        headers: {
            "X-CSRFToken": csrftoken,
        },
        success: function (result) {
            if (result.data.length > 0) {
                $.each(result.data, function (index, value) {
                    var isi =
                    '<div class="col s12 m4 l4 xl4 " style="height:30em">' +
                    '<div class="card">' +
                    '<div class="card-image">' +
                    '<img src="' + value.base64_pic + '" style="width:100%; height:20em">' +
                    '</div>' +
                    '<div class="card-content">' +
                    '<span class="card-title">' + value.nama_barang + '</span>' +
                    '<p>Penjual   = ' + value.penjual + '</p>' +
                    '<p>Kategori   = ' + value.kategori_barang + '</p>' +
                    '<p>Harga    = Rp ' + value.harga + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                    $("#divnya").append(isi)
                })
            } else {
                $("#divnya").append('<h1>Belum ada barang yang dijual :(</h1>')
            }
        },
        error: function (a, b) {
            console.log(a)
            console.log(b)
        }
    })
}