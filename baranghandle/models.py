from django.db import models
from userhandle.models import ReUseUser
from django.template.defaultfilters import slugify

class BarangModel(models.Model):

    Choice = {
       ("Hobi", "Hobi"),
       ("Furniture", "Furniture"),
       ("Kendaraan", "Kendaraan"),
       ("Aksesoris", "Aksesoris"),
       ("Alat Tulis", "Alat Tulis"),
       ("Elektronik", "Elektronik"),
       ("Pakaian", "Pakaian"), 
    }

    penjual = models.ForeignKey(ReUseUser, related_name = "penjual_barang", on_delete = models.CASCADE)
    nama_barang = models.CharField(max_length=250)
    picture = models.ImageField(upload_to = "barang_pics", blank = False)
    kategori_barang = models.CharField(choices=Choice, max_length=100)
    deskripsi = models.TextField()
    harga = models.IntegerField(default=0)
    slug = models.SlugField(unique=True)
    base64_pic = models.TextField(blank=False)
    terbeli = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.penjual.username + "/" + self.nama_barang)
        super(BarangModel,self).save(*args,**kwargs)

class Keranjang(models.Model):
    pembeli = models.ForeignKey(ReUseUser, related_name = "pembeli", on_delete = models.CASCADE)
    barang = models.ManyToManyField(BarangModel)

    def __str__(self):
        return self.pembeli.username