from django.urls import path
from .views import * 

app_name = "barang"
# /
urlpatterns = [
    path("",homepage, name="home"),
    path("barang.json",homepage_json, name="home_json"),
    path("detail_barang/<slug_barang>", detail_barang, name="barang"),
    path("jual", jual_barang, name="jual"),
    path("get_barang", get_user_barang, name="get_barang"),
    path("list_barang", list_barang, name="listbarang"),
    path("pembelian", pembelian, name="pembelian"),
    path("pembelian.json", pembelian_json, name="pembelian_json"),
    path("beli/<slug_barang>", ke_pembelian, name="beli"),
]