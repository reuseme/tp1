from django.contrib.auth.decorators import login_required
from baranghandle.models import BarangModel, Keranjang
from django.shortcuts import render_to_response
from django.shortcuts import render, redirect
from django.template import RequestContext
from baranghandle.forms import BarangForm
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from userhandle.models import ReUseUser
from django.http import JsonResponse
from django.contrib import messages
from django.utils.html import escape
from django.urls import reverse

# /barang.json
def homepage_json(request):
    barang_all = list(BarangModel.objects.filter(terbeli = False).values())
    temp = []
    for i in barang_all:
        penjualnya = ReUseUser.objects.get(id = i['penjual_id'])
        i['penjual'] = escape(penjualnya.username)
        i['nama_barang'] = escape(i['nama_barang'])
        i['kategori_barang'] = escape(i['kategori_barang'])
        i['deskripsi'] = escape(i['deskripsi'])
        i['harga'] = escape(i['harga'])
        i['slug'] = escape(i['slug'])
        temp.append(i)
    return JsonResponse({"data" : temp})

# /
def homepage(request):
    barang_all = BarangModel.objects.filter(terbeli = False)
    if request.method == 'POST':
        query1 = request.POST.get('q')
        query2 = request.POST.get('kategori')
        if query1 :
            result = list(BarangModel.objects.filter(nama_barang__icontains=query1, terbeli=False).values())
            temp = []
            for i in result:
                penjualnya = ReUseUser.objects.get(id = i['penjual_id'])
                i['penjual'] = escape(penjualnya.username)
                i['nama_barang'] = escape(i['nama_barang'])
                i['kategori_barang'] = escape(i['kategori_barang'])
                i['deskripsi'] = escape(i['deskripsi'])
                i['harga'] = escape(i['harga'])
                i['slug'] = escape(i['slug'])
                temp.append(i)
            return JsonResponse({"data" : result})

        elif query2 :
            result = list(BarangModel.objects.filter(kategori_barang__icontains=query2, terbeli=False).values())
            temp = []
            for i in result:
                penjualnya = ReUseUser.objects.get(id = i['penjual_id'])
                i['penjual'] = escape(penjualnya.username)
                i['nama_barang'] = escape(i['nama_barang'])
                i['kategori_barang'] = escape(i['kategori_barang'])
                i['deskripsi'] = escape(i['deskripsi'])
                i['harga'] = escape(i['harga'])
                i['slug'] = escape(i['slug'])
                temp.append(i)
            return JsonResponse({"data" : result})

        return JsonResponse({"data" : 'hai'})
    return render(request,"baranghanlde/homepage.html")

# /jual
@login_required
def jual_barang(request):
    import base64
    form = BarangForm(request.POST, request.FILES)
    
    if request.method == "POST":
        if form.is_valid():
            instance = form.save(commit=False)
            instance.penjual = request.user
            with form.cleaned_data["picture"].open('rb') as file:
                instance.base64_pic = "data:image/png;base64,{}".format(base64.b64encode(file.read()).decode())
                instance.save()
    
            return redirect("barang:home")
    return render(request, "baranghanlde/jual.html", {
        "form" : BarangForm()
    })

# /detail_barang/{slug}
def detail_barang(request,slug_barang):
    
    barang_detail = BarangModel.objects.get(slug = slug_barang)
    
    return render(request, "baranghanlde/detail_barang.html",{
        "barang":barang_detail,
    })

# /list_barang
@login_required
def list_barang(request):
    if request.method == 'POST':
        data = request.POST.get("data")
        usernya = ReUseUser.objects.get(id=data)
        barangku = list(BarangModel.objects.filter(penjual=usernya).values())
        temp = []
        for i in barangku:
            penjualnya = ReUseUser.objects.get(id = i['penjual_id'])
            i['penjual'] = escape(penjualnya.username)
            i['nama_barang'] = escape(i['nama_barang'])
            i['kategori_barang'] = escape(i['kategori_barang'])
            i['deskripsi'] = escape(i['deskripsi'])
            i['harga'] = escape(i['harga'])
            i['slug'] = escape(i['slug'])
            temp.append(i)

        return JsonResponse({'data' : temp})
    return render(request,"baranghanlde/list_barang.html")

# /get_barang
@login_required
def get_user_barang(request):
    semua_barang = BarangModel.objects.filter(penjual=request.user)
    banyak_barang = semua_barang.count()
    barang_lst = []
    for barang in semua_barang:
        item = {
            "penjual"   : barang.penjual.username,
            "nama_barang" : barang.nama_barang,
            "harga"     : barang.harga,
            "kategori"  : barang.kategori_barang,
            "deskripsi" : barang.deskripsi, 
            "gambar"    : barang.base64_pic,
            "terjual"   : barang.terbeli,
        }
        barang_lst.append(item)

    response = JsonResponse(barang_lst, safe=False)
    return response

# /pembelian
@login_required
def pembelian(request):
    try :
        barang_keranjang = Keranjang.objects.get(pembeli=request.user)
        return render(request,"baranghanlde/list_keranjang.html")
    except Exception as e :
        add_obj_keranjang = Keranjang(pembeli=request.user)
        add_obj_keranjang.save()
        return render(request,"baranghanlde/list_keranjang.html")

# /pembelian.json
def pembelian_json(request):
    try:
        data = request.GET.get('data')
        usernya  = ReUseUser.objects.get(id=data)
        data = list(Keranjang.objects.get(pembeli=usernya).barang.values())
        temp = []
        for i in data:
            penjualnya = ReUseUser.objects.get(id = i['penjual_id'])
            i['penjual'] = escape(penjualnya.username)
            i['nama_barang'] = escape(i['nama_barang'])
            i['kategori_barang'] = escape(i['kategori_barang'])
            i['deskripsi'] = escape(i['deskripsi'])
            i['harga'] = escape(i['harga'])
            i['slug'] = escape(i['slug'])
            temp.append(i)
        return JsonResponse({'data' : temp})
    except Exception as e:
        return JsonResponse({'data' : str(e)})

# /beli/{slug}
@login_required
def ke_pembelian(request,slug_barang):
    
    barang_incaran = BarangModel.objects.get(slug = slug_barang)
    pembeli = ReUseUser.objects.get(username=request.user.username)
    try :
        keranjang = Keranjang.objects.get(pembeli=pembeli)

        if request.method == "POST" and barang_incaran.penjual != pembeli:
            
            keranjang.barang.add(barang_incaran)
            keranjang.save()
            barang_incaran.terbeli = True
            barang_incaran.save()
            messages.success(request, "Barang berhasil dibeli")

        else :
            messages.warning(request, "Ini adalah barang jualan anda")

    except Keranjang.DoesNotExist as e:
        add_obj_keranjang = Keranjang(pembeli = request.user)
        add_obj_keranjang.save()
        if request.method == "POST" and barang_incaran.penjual.username != add_obj_keranjang.pembeli.username:
            add_obj_keranjang.barang.add(barang_incaran)
            add_obj_keranjang.save()
            barang_incaran.terbeli = True
            barang_incaran.save()
            messages.success(request, "Barang berhasil dibeli")
        else:
            messages.warning(request, "Ini adalah barang jualan anda")            

    return redirect(reverse("barang:barang", kwargs={"slug_barang":slug_barang}) + '#first')


@login_required
def keluar_keranjang(request,barang):
    
    barang_incaran = BarangModel.objects.get(slug=barang)
    pembeli = ReUseUser.objects.get(username=request.user.username)
    keranjang = Keranjang.objects.get(pembeli=pembeli)
    
    if request.method == "POST":
        keranjang.barang.remove(barang_incaran)
    return redirect("barang:barang")
