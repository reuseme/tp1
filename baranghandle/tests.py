from django.test import TestCase, Client
from baranghandle.models import *
from userhandle.models import ReUseUser
from baranghandle.forms import BarangForm
class TestBarangModel(TestCase):
    def setUp(self):
        ReUseUser.objects.create(
            nama_panjang = "anggardha febriano",
            no_identitas = "123456789",
            username = "anggardhanoano",
            password = "test123",
            email = "anggardhanoano@gmail.com",
            alamat = "pesona depok 1,blok ad 2",
            nama_bank = "Bank Negara Berkembang",
            no_rekening = "000123456"
        )

    def test_isi_gudang(self):
        user = ReUseUser.objects.get(id=1)
        BarangModel.objects.create(
            penjual = user,
            nama_barang = "sepeda",
            kategori_barang = "Kendaraan",
            deskripsi = "sepeda ini dibuat hanya untuk pajangan saja ya hehehehe",
            harga = 100000
        )
        barang = BarangModel.objects.get(nama_barang="sepeda")
        self.assertEqual(barang.id, 1)
        self.assertEqual(barang.kategori_barang, "Kendaraan")
        self.assertEqual(barang.harga, 100000)

    def test_buat_barang(self):
        user = ReUseUser.objects.get(id=1)
        orang = BarangModel(
            penjual = user,
            nama_barang = "sepeda",
            kategori_barang = "Kendaraan",
            deskripsi = "sepeda ini dibuat hanya untuk pajangan saja ya hehehehe",
            harga = 100000
        )
        orang.save()
        self.assertEqual(ReUseUser.objects.all().count(), 1)


class TestBarangViews(TestCase):
    
    def setUp(self):
        ReUseUser.objects.create(
            nama_panjang = "anggardha febriano",
            no_identitas = "123456789",
            username = "anggardhanoano",
            password = "test123",
            email = "anggardhanoano@gmail.com",
            alamat = "pesona depok 1,blok ad 2",
            nama_bank = "Bank Negara Berkembang",
            no_rekening = "000123456"
        )
    # untuk homepage
    def test_homepage(self):
        self.client = Client()
        response = self.client.get("/")
        self.assertTemplateUsed(response, "baranghanlde/homepage.html")

    def test_html(self):
        self.client = Client()
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200, "page error")

    def test_contain_name(self):
        response =  Client().get("/")
        html = response.content.decode("utf8")
        self.assertIn("Barang Terbaru", html)

    def test_jual_data(self):
        data = {
            "penjual": "anggardhanoano",
            "nama_barang" : "Kaos Oblong",
            "kategori_barang" : "Pakaian",
            "deskripsi" : "masih bagus, baru sekali dipake gan,blom dicuci",
            "harga" : 50000,
            "picture":"asdjaksfa.jpg"
        }
        response = Client().post("/jual", data)
        self.assertEqual(response.status_code, 302)