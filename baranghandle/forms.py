from django import forms
from baranghandle.models import BarangModel
from django.utils.text import slugify

class BarangForm(forms.ModelForm):
    kategori_choice = {
        ("Hobi", "Hobi"),
       ("Furniture", "Furniture"),
       ("Kendaraan", "Kendaraan"),
       ("Aksesoris", "Aksesoris"),
       ("Alat Tulis", "Alat Tulis"),
       ("Elektronik", "Elektronik"),
       ("Pakaian", "Pakaian"), 
    }
    kategori_barang = forms.Select(choices=kategori_choice)
    class Meta:
        model = BarangModel
        fields = ("nama_barang", "picture", "kategori_barang","deskripsi","harga")
