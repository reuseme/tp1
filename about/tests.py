from django.test import TestCase

# Create your tests here.

def test_homepage(self):
    self.client = Client()
    response = self.client.get("/")
    self.assertTemplateUsed(response, "about/testi.html")

def test_html(self):
    self.client = Client()
    response = self.client.get("/")
    self.assertEqual(response.status_code, 200, "page error")

def test_contain_name(self):
    response =  Client().get("/")
    html = response.content.decode("utf8")
    self.assertIn("Testimoni", html)

