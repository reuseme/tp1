from django import forms
from about.models import Testi

class TestiForm(forms.ModelForm):

    testimoni = forms.CharField(label="", widget=forms.TextInput(
    attrs = {
            "id" : "testimoni",
            "required": "true",
            }
    ))
    class Meta:
        model = Testi
        fields = ("testimoni",)
