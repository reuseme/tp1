from django.shortcuts import render, redirect
from userhandle.models import ReUseUser
from about.models import Testi
from django.utils.html import escape
from about.forms import TestiForm
from django.http import JsonResponse

# Create your views here.

def about(request):

    return render(request, "about/testi.html", 
        {
            "form" : TestiForm()
        }
    )

def submit_testi(request):

    form = TestiForm(request.POST)
    
    if request.method == "POST":
        if form.is_valid():
            instance = form.save(commit=False)
            instance.penulis = request.user
            instance.save()

    semua_testi = list(Testi.objects.all().values())[::-1]
    temp = []
    for i in semua_testi:
        penjualnya = ReUseUser.objects.get(id = i['penulis_id'])
        i['penulis'] = escape(penjualnya.username)
        i['testimoni'] = escape(i['testimoni'])
        temp.append(i)

    return JsonResponse({'data' : temp})
