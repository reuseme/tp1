from django.urls import path
from about.views import * 

app_name = "about"

urlpatterns = [
    path("",about, name="home"),
    path("submit_testi", submit_testi, name="submit")
]
